{ pkgs, host, vars, ... }:

{
    home-manager.users.${vars.user} = {
        gtk = {                                 # Theming
            enable = true;
            theme = {
                name = "Gruvbox-dark";
                package = pkgs.gruvbox-dark-gtk;
            };
            iconTheme = {
                name = "Gruvbox-icons";
                package = pkgs.gruvbox-plus-icons;
            };
            cursorTheme = {
                name = "Nordzy-cursors";
                package = pkgs.nordzy-cursor-theme;
            };
            font = {
                name = "FantasqueSansMono";
            };
            #            gtk2 = {
            #    configLocation = "/home/${vars.user}/.gtkrc-2.0";
            #};
        };
        qt = {
            enable = true;
            platformTheme.name = "gtk";
            style = {
                name = "gtk2";
                package = pkgs.gruvbox-dark-gtk;
            };
        };

    };
    environment = { 
        # Necessary Packages for theming to work
        systemPackages = with pkgs; [
            #libsForQt5.qtstyleplugin-kvantum
            #libsForQt5.qt5ct
            lxappearance-gtk2
        ];
    };
}
