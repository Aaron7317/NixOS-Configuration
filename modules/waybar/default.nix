{ config, pkgs, vars, ... }: 

{
    environment = {
        systemPackages = with pkgs; [
            waybar
            imagemagick
            wirelesstools
            sysstat
            rofi-power-menu
        ];
    };
    home-manager.users.${vars.user} = {
        xdg.configFile = {
            waybar = {
                source = ./waybar;
                recursive = true;
            };
        };
    };
}
