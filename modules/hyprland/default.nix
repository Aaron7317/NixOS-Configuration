{ config, pkgs, lib, vars, ... }:
{

    programs = {
        hyprland = {
            enable = true;
        };
    };

    xdg.portal = {
        enable = true;
        extraPortals = [ pkgs.xdg-desktop-portal-hyprland ];
    };

    home-manager.users.${vars.user} = {
        xdg.configFile = {
            hypr = {
                source = ./hypr;
                recursive = true;
            };
        };
    };

    environment = {

        systemPackages = with pkgs; [

            jq
            socat
            wf-recorder
            pavucontrol
            pamixer
            ripgrep
            hyprpicker

            # Notifications
            dunst
            libnotify

            # Screenshots
            grim
            slurp

            # Wallpaper
            mpvpaper
            waypaper
            swaybg

            wlr-randr
            wlsunset
            wl-gammactl
            brightnessctl
        ];

        variables = {
            XDG_CURRENT_DESKTOP="Hyprland";
            XDG_SESSION_TYPE="wayland";
            XDG_SESSION_DESKTOP="Hyprland";
        };

        #systemd.sleep.extraConfig = ''
            #AllowSuspend=yes
            #AllowHibernation=no
            #AllowSuspendThenHibernate=no
            #AllowHybridSleep=yes
        #'';                                             # Required for clamshell mode (see script bindl lid switch and script in home.nix)
    };

}
