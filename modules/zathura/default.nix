{ config, pkgs, vars, ... }: 

{
    environment = {
        systemPackages = with pkgs; [
            zathura
        ];
    };
    home-manager.users.${vars.user} = {
        xdg.configFile = {
            zathura = {
                source = ./zathura;
                recursive = true;
            };
        };
    };
}
