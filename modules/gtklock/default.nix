{ pkgs, vars, lib, ... }:

{
    
    security.pam.services.gtklock.text = lib.readFile "${pkgs.gtklock}/etc/pam.d/gtklock";

    environment = {
        systemPackages = with pkgs; [
            gtklock
        ];
    };

}
