# NixOS-Configuration
This is the files for my complete NixOS configuration on all of my systems. This is a work in progress; I plan to add more documentation, scripts, machines, and script inputs. Contributions are welcome.




### Notes and Tricks
- Make sure sure to stage files with `git add .` before adding a module or NixOS won't see it when you update
