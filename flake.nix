{
    description = "A Flake for managing the entire nixos configuration";

    inputs = {
        # Nixpkgs unstable branch
        nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
        nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    
        # Home Manager
        home-manager = {
            #url = "github:nix-community/home-manager";
            #inputs.nixpkgs.follows = "nixpkgs-unstable";
            url = "github:nix-community/home-manager/release-24.05";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        # Nixos Hardware Optimizations
        #nixos-hardware = {
        #    url = "github:NixOS/nixos-hardware/master";
        #};

    };

    outputs = inputs @ { self, nixpkgs, nixpkgs-unstable, home-manager, ... }:
        let
	    vars = {
        	user = "aaron";
		    terminal = "foot";
            location = "$HOME/.flake";
		    editor = "nvim";
	    };
        in 
	    {
            nixosConfigurations = (
                import ./hosts {
                    inherit (nixpkgs) lib;
                    inherit inputs nixpkgs nixpkgs-unstable home-manager vars; }
            );
        };
}
