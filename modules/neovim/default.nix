{ pkgs, lib, ... }:

{
    programs = {
        hyprland = {
            enable = true;
        };
    };


    environment = {

        systemPackages = with pkgs; [
            wl-clipboard
	        lua-language-server
            clang-tools
            clang
        ];
    };
}
