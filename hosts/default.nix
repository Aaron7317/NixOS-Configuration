{ lib, inputs, nixpkgs, nixpkgs-unstable, home-manager, vars, ... }:

let
    system = "x86_64-linux";

    pkgs = import nixpkgs {
        inherit system;
        config = {
            allowUnfree = true;
        };
    };

    unstable = import nixpkgs-unstable {
        inherit system;
        config = {
            allowUnfree = true;
        };
    };

    lib = nixpkgs.lib;
in 
{
    framework-laptop = lib.nixosSystem {
        inherit system;
        specialArgs = {
            inherit inputs pkgs unstable vars;
            host = {
                hostName = "aaron";
            };
        };
        modules = [
            ./framework-laptop
            ./configuration.nix 
            
            home-manager.nixosModules.home-manager {
                home-manager.backupFileExtension = ".home-manager-backup";
                home-manager.useGlobalPkgs = true;
                home-manager.useUserPackages = true;
            }
        ];
    };
    main-pc = lib.nixosSystem {
        inherit system;
        specialArgs = {
            inherit inputs pkgs unstable vars;
            host = {
                hostName = "aaron";
            };
        };
        modules = [
            ./main-pc
            ./configuration.nix 
            
            home-manager.nixosModules.home-manager {
                #home-manager.backupFileExtension = ".home-manager-backup";
                home-manager.useGlobalPkgs = true;
                home-manager.useUserPackages = true;
            }
        ];
    };
}
