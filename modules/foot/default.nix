{ pkgs, vars, ... }:

{
    home-manager.users.${vars.user} = {
        programs = {
            foot = {
                enable = true;
                server.enable = true;
            };
        };
        xdg.configFile = {
            foot = {
                source = ./foot;
                recursive = true;
            };
        };
    };
}
