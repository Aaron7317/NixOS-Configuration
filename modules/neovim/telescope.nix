{
    programs.nixvim.plugins.telescope = {
        enable = true;
        settings = {
            pickers.find_files = {
                hidden = true;
            };
        };
        keymaps = {
            "<leader>ff" = "find_files";
            "<leader>fg" = "live_grep";
            "<leader>fb" = "buffers";
            "<leader>fh" = "help_tags";
        };
    };
}
