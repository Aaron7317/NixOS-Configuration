{ config, lib, pkgs, unstable, inputs, vars, ... }:

{
    imports = [
        ../modules
    ];

    # Main user
    users.users.${vars.user} = {
        isNormalUser = true;
        extraGroups = [ "wheel" "kvm" "libvirtd" "input" "dialout"];
    };

    networking = {
        hostName = "aaron";
        networkmanager.enable = true;
    };

    # Select internationalisation properties.
    i18n.defaultLocale = "en_US.UTF-8";

    i18n.extraLocaleSettings = {
        LC_ADDRESS = "en_US.UTF-8";
        LC_IDENTIFICATION = "en_US.UTF-8";
        LC_MEASUREMENT = "en_US.UTF-8";
        LC_MONETARY = "en_US.UTF-8";
        LC_NAME = "en_US.UTF-8";
        LC_NUMERIC = "en_US.UTF-8";
        LC_PAPER = "en_US.UTF-8";
        LC_TELEPHONE = "en_US.UTF-8";
        LC_TIME = "en_US.UTF-8";
    };

    # Configure keymap in X11
    services.xserver.xkb = {
        layout = "us";
        variant = "";
    };

    # Enable CUPS to print documents.
    services.printing.enable = true;

    # Enable sound with pipewire.
    sound.enable = true;
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
        # If you want to use JACK applications, uncomment this
        #jack.enable = true;

        # use the example session manager (no others are packaged yet so this is enabled by default,
        # no need to redefine it in your config for now)
        #media-session.enable = true;
    };

    # Fonts to be installed
    fonts.packages = with pkgs; [
        fantasque-sans-mono
        nerdfonts
        corefonts
    ];

    # Environment settings
    environment = {
        variables = {
            TERMINAL = "${vars.terminal}";
            EDITOR = "${vars.editor}";
            VISUAL = "${vars.editor}";
        };
        systemPackages = with pkgs; [

            # File Management
            vifm-full

            # Virtualization
            virt-manager
            qemu

            # Drone
            #betaflight-configurator

            # VPN
            openconnect
            gp-saml-gui

            # Utility
            fastfetch
            htop
            unzip
            zip
            killall
            vim
            pciutils
            usbutils
            wget
            weechat

            # Media
            mpv

            # Browser
            brave

            # Productivity
            gimp
            godot_4
            prusa-slicer
            blender
            krita
            love
            (tic-80.override { withPro = true; })
            tiled
            xournalpp
            calibre

            # Security
            keepassxc
            lynis
            aircrack-ng
            websploit
            ghidra

            # Gaming
            steam
            discord
            minetest
            xonotic
            cataclysm-dda
            mindustry-wayland
            devilutionx

            # Programming
            gcc
            gdb
            cmake
            gnumake
            git
            jupyter
	        cargo
            nodePackages.npm
            ncurses
            bear
            raylib

            # neovim/dev stuff
            wl-clipboard
            clang-tools
            clang
        ] ++ 
        (with unstable; [
            betaflight-configurator
            neovim
        ]);
    };

    nix = {
        settings = {
            auto-optimise-store = true;
        };
        gc = {
            automatic = true;
            dates = "weekly";
            options = "--delete-older-than 4d";
        };
        package = pkgs.nixVersions.git;
        registry.nixpkgs.flake = inputs.nixpkgs;
        extraOptions = ''
            experimental-features = nix-command flakes
            keep-outputs          = true
            keep-derivations      = true
        '';
    };

    # System info
    system = {
        # This value determines the NixOS release from which the default
        # settings for stateful data, like file locations and database versions
        # on your system were taken. It‘s perfectly fine and recommended to leave
        # this value at the release version of the first install of this system.
        # Before changing this value read the documentation for this option
        # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
        stateVersion = "22.11"; # Did you read the comment?
    };
}
