{
    programs.nixvim.plugins.lsp = {
        enable = true;
        servers = {
            bashls.enable = true;
            clangd.enable = true;
            nixd.enable = true;
            html.enable = true;
            tsserver.enable = true;
            pyright.enable = true;
            gdscript.enable = true;
            lua-ls = {
                enable = true;
                settings = {
                    telemetry.enable = true;
                    #workspace.libary = "${3rd}/love2d/library";
                    workspace.library = [
                        "\${3rd}/love2d/library"
                    ];
                };
            };
        };
        keymaps.lspBuf = {
            "gd" = "definition";
            "gD" = "references";
            "gt" = "type_definition";
            "gi" = "implementation";
            "K" = "hover";
        };
    };
}
