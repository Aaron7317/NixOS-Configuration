{ config, pkgs, vars, ... }:

{
    imports = [
        ./theme
        ./hyprland
        ./foot
        ./greetd
        ./rofi
        ./gtklock
        ./waybar
        ./zathura
    ];

    home-manager.users.${vars.user} = {

        programs = {
            # Let Home Manager install and manage itself.
            home-manager = {
                enable = true;
            };
        };

        home = {
            username = "${vars.user}";
            homeDirectory = "/home/${vars.user}";
            stateVersion = "22.11";
        };
    };
}
