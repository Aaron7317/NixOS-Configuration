#!/usr/bin/env bash

HERE="$(dirname "$(readlink -f "${0}")")"

printf "Please select target:\n"
select d in $HERE/hosts/*/; do test -n "$d" && break; echo ">>> Invalid Selection"; done
TARGET="$(basename $d)"

sudo nix-channel --update
nix flake update $HERE
nix flake archive
sudo nixos-rebuild switch --flake "$HERE#$TARGET"
